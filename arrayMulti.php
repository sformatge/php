<?php
	function genera($n) {
		for ($i = 0; $i < $n; $i++) {
			for ($j = 0; $j < $n; $j++) {
				if ($i == $j) {
					$array[$j][$i] = "*";
				} elseif ($i < $j) {
					$array[$j][$i] = rand(10,20);
				} else {
					$array[$j][$i] = $i + $j;
				}
			}
		}
		return $array;
	}
	function gira($array) {
		for ($i = 0; $i < sizeof($array); $i++) {
			for ($j = 0; $j < sizeof($array); $j++) {
				$girat [$i][$j] = $array [$j][$i];
			}	
		}
		return $girat;
	}
	function mostra($array) {
		echo "<table style='border: solid 1px; border-collapse: collapse'>";
		foreach ($array as $fila) {
			echo "<tr style='border: solid 1px; border-collapse: collapse'>";
			foreach ($fila as $col) {
				echo "<td style='border: solid 1px; border-collapse: collapse'>";
				echo $col; 
				echo "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}

$show = genera(6);
mostra($show);
echo "</br>";
mostra(gira($show));